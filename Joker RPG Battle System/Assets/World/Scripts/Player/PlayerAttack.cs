using UnityEngine;

public class PlayerAttack : StateBase<PlayerState, PlayerController>
{
    public override void OnEnter()
    {
        Controller.Attack();
    }

    public override void OnExit()
    {
    }

    public override void OnUpdate()
    {
        // 由于在此触发的更新，必然是处于“攻击”状态。
        // 所以，这里的检测是只能检测到第2段开始的多段攻击。
        // 如果不加这里，则进行不了多段攻击，因为状态已经改变了。
        if (Controller.CheckAttack())
            Controller.Attack();

        if (Controller.CurrentSkill.ReleaseData.CanRotate)
        {
            //// 旋转
            //var rotate = new Vector3(0, Controller.Input.Horizontal, 0);
            //Controller.transform.Rotate(rotate * Time.deltaTime * 60);

            PlayerRotate();
        }
    }

    #region 角色旋转

    private void PlayerRotate()
    {
        //输入向量
        var inputDirection = new Vector3(Controller.Input.Horizontal, 0f, Controller.Input.Vertical);

        //up轴向
        var upAxis = -Physics.gravity.normalized;

        //相机输入方向修正
        var moveDirection = CameraDirectionProcess(inputDirection, upAxis);

        if (inputDirection.magnitude > PlayerInput.INPUT_EPS)
            RotateProcess(Controller.transform, moveDirection, upAxis);
    }

    private Vector3 CameraDirectionProcess(Vector3 inputDirection, Vector3 upAxis)
    {
        //获取主相机
        var mainCamera = Camera.main;

        //不同重力的up轴修正
        var quat = Quaternion.FromToRotation(mainCamera.transform.up, upAxis);

        //转换forward方向
        var cameraForwardDirection = quat * mainCamera.transform.forward;

        //转换输入向量方向
        var moveDirection = Quaternion.LookRotation(cameraForwardDirection, upAxis) * inputDirection.normalized;

        return moveDirection;
    }

    private readonly float _rotateSpeed = 20f;
    private void RotateProcess(Transform transform, Vector3 moveDirection, Vector3 upAxis)
    {
        //投影到up平面上
        moveDirection = Vector3.ProjectOnPlane(moveDirection, upAxis);

        //得到移动方向代表的旋转
        var playerLookAtQuat = Quaternion.LookRotation(moveDirection, upAxis);

        //更新插值
        transform.rotation = Quaternion.Lerp(transform.rotation, playerLookAtQuat, _rotateSpeed * Time.deltaTime);
    }

    #endregion
}
