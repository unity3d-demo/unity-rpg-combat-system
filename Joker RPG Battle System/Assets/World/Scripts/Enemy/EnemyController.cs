using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class EnemyController : FSMController<EnemyState>
{
    private int _hp = 100;

    /// <summary>
    /// 是否受击中。
    /// </summary>
    private bool _isHurt = false;

    /// <summary>
    /// 受击力量。
    /// </summary>
    private Vector3 _hurtVelocity;

    /// <summary>
    /// 受击过渡时间。
    /// </summary>
    private float _hurtTime;

    /// <summary>
    /// 当前时间。
    /// </summary>
    private float _currentHurtTime;

    public EnemyModel Model { get; private set; }
    public CharacterController CharacterController { get; private set; }

    private void Awake()
    {
        CharacterController = GetComponent<CharacterController>();
        Model = transform.GetComponentInChildren<EnemyModel>();
        Model.Init(this);

        ChangeState<EnemyMove>(EnemyState.Move);
    }

    protected override void Update()
    {
        base.Update();

        if (_isHurt)
        {
            _currentHurtTime += Time.deltaTime;

            // 使用 _hurtTime （秒）的时间，移动了 _hurtVelocity 距离。
            CharacterController.Move(_hurtVelocity * Time.deltaTime / _hurtTime);

            if (_currentHurtTime >= _hurtTime)
                _isHurt = false;
        }
        else
        {
            // 给角色一个向下的重力。
            CharacterController.Move(new Vector3(0, -9f, 0) * Time.deltaTime);
        }
    }

    /// <summary>
    /// 受伤。
    /// </summary>
    public void Hurt(SettingSkillHitData hitData, Transform sourceTransform)
    {
        // 僵直与播放动画。
        Model.PlayHurtAnimation();

        // 僵直：无法进行任何操作。
        // 实现：当播放受伤动画后，如果没有触发受伤结束，就不会转回待机状态，所以不可以做任何动作。
        // 函数：多少秒后执行指定的函数。
        // 所以：这里表示播完受伤动画的 1 秒后，执行“受伤结束”回到待机状态，也就是所谓的 1 秒僵直。
        // 注意：在执行 Invoke 函数前，先要取消之前可能存在的相同操作，否则僵直就会无限叠加。
        CancelInvoke("HurtOver");
        Invoke("HurtOver", hitData.HardTime);

        // 击退或者击飞。
        _isHurt = true;
        // 击退或者击飞的方向是：玩家所在位置与方向 + 施力方向。
        _hurtVelocity = sourceTransform.TransformDirection(hitData.RepelVelocity);
        _hurtTime = hitData.RepelTransitionTime;
        _currentHurtTime = 0;

        // 生命值减少。
        _hp -= hitData.DamageValue;
    }

    public void HurtOver()
    {
        Model.StopHurtAnimation();
    }
}

public enum EnemyState
{
    None = 0,
    Move = 1,
    Attack = 2,
}