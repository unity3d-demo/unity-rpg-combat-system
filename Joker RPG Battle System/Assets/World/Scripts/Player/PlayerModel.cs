
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerModel : MonoBehaviour
{
    /// <summary>
    /// 当前使用中的技能配置数据。
    /// </summary>
    private SettingSkillData _settingSkillData;

    private PlayerController _playerController;
    private Animator _animator;

    [SerializeField]
    private WeaponCollider[] _weaponColliders;

    /// <summary>
    /// 是否装备中。
    /// </summary>
    public bool IsEquip { get; set; } = true;

    /// <summary>
    /// 是否可以切换技能。
    /// </summary>
    public bool CanSkillSwitch { get; private set; } = true;

    public void Init(PlayerController playerController)
    {
        _playerController = playerController;
        _animator = GetComponent<Animator>();

        if (_weaponColliders != null && _weaponColliders.Length > 0)
        {
            foreach (var weaponCollider in _weaponColliders)
                weaponCollider.Init(this);
        }
    }

    public void PlayAudio(AudioClip audioClip)
    {
        _playerController.PlayAudio(audioClip);
    }

    /// <summary>
    /// 屏幕震动。
    /// </summary>
    public void ScreenImpulse()
    {
        _playerController.ScreenImpulse();
    }

    public void UpdateMove(float horizontal, float vertical)
    {
        if (IsEquip)
            _animator.SetBool("Equip", true);

        _animator.SetFloat("Horizontal", horizontal);
        _animator.SetFloat("Vertical", vertical);
    }

    public void StartAttack(SettingSkillData skillData)
    {
        _currentHitIndex = 0;

        CanSkillSwitch = false;

        _settingSkillData = skillData;
        _animator.SetTrigger(_settingSkillData.TriggerName);

        // 释放技能时生成特效。
        SpwanObject(_settingSkillData.ReleaseData.Spawn);
        // 播放音效。
        PlayAudio(_settingSkillData.ReleaseData.AudioClip);
    }

    #region 动画事件（在动画上加上事件即可，不需要拖入对象）

    #region 事件-技能开始造成伤害

    /// <summary>
    /// 当前伤害索引。
    /// </summary>
    private int _currentHitIndex = 0;

    /// <summary>
    /// 技能开始造成伤害。
    /// </summary>
    /// <param name="weaponIndex"></param>
    private void StartSkillHit(int weaponIndex)
    {
        var hit = _settingSkillData.HitDatas[_currentHitIndex++];

        // 启用刀光拖尾。
        // 启用伤害检测触发器。
        _weaponColliders[weaponIndex].StartAttack(hit);
        // 释放技能时，生成每段攻击的特效（每段攻击的特效都不一样）。
        SpwanObject(hit.Spawn);
        // 播放每段攻击的音效。
        PlayAudio(hit.AudioClip);
    }

    private void SpwanObject(SettingSkillSpawnObjectData spawn)
    {
        if (spawn == null)
            return;

        if (spawn.Prefab == null)
            return;

        var skillObject = Instantiate(spawn.Prefab, null);

        // 位置 = 碰撞体接触点 + 偏移量。
        skillObject.transform.position = transform.position + spawn.Position;
        // 旋转偏移（以玩家角色的角度为目标）。
        skillObject.transform.eulerAngles = _playerController.transform.eulerAngles + spawn.Rotation;
        // 播放音效。
        PlayAudio(spawn.AudioClip);
    }

    #endregion

    #region 技能停止造成伤害

    /// <summary>
    /// 技能停止造成伤害。
    /// </summary>
    private void StopSkillHit(int weaponIndex)
    {
        // 停止刀光拖尾。
        // 停止伤害检测触发器。
        _weaponColliders[weaponIndex].EndAttack();
    }

    #endregion

    #region 技能切换

    /// <summary>
    /// 技能切换。
    /// </summary>
    private void SkillCanSwitch()
    {
        CanSkillSwitch = true;
    }

    #endregion

    #region 技能执行结束

    /// <summary>
    /// 技能执行结束。
    /// </summary>
    private void SkillOver(string skillName)
    {
        // 为了避免 2 个技能之间切换的时候，第1个技能依然会触发此事件而出现重复攻击 1 的 BUG。
        if (_settingSkillData.Name != skillName)
            return;

        SpwanObject(_settingSkillData.EndData.Spawn);

        _animator.SetTrigger(_settingSkillData.OverTriggerName);

        _playerController.CurrentStandAttackIndex = 0;
        _playerController.ChangeState<PlayerMove>(PlayerState.Move);
        CanSkillSwitch = true;
    }

    #endregion

    /// <summary>
    /// 释放技能时产生的摄像机位移效果。
    /// </summary>
    /// <param name="index">索引。</param>
    private void CameraMoveForAttack(int index)
    {
        if (_settingSkillData == null || _settingSkillData.CameraMoveDatas == null)
            return;

        if (_settingSkillData.CameraMoveDatas.Length <= index)
            return;

        var config = _settingSkillData.CameraMoveDatas[index];

        if (config == null)
            return;

        _playerController.CameraMoveForAttack(config.Offset, config.RunTime, config.BackTime);
    }

    private void CharacterAttackMove(int index)
    {
        if (_settingSkillData == null || _settingSkillData.CharacterMoveDatas == null)
            return;

        if (_settingSkillData.CharacterMoveDatas.Length <= index)
            return;

        var config = _settingSkillData.CharacterMoveDatas[index];

        if (config == null)
            return;

        _playerController.CharacterAttackMove(config.Offset, config.RunTime);
    }

    private void SpawnSkillObjects(int index)
    {
        SpwanObject(_settingSkillData.Spawns[index]);
    }

    #endregion
}
