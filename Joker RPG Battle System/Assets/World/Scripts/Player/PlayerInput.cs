using UnityEngine;

public class PlayerInput
{
    public const float INPUT_EPS = 0.2f;

    /// <summary>
    /// 按下此按钮时，角色跑步。
    /// </summary>
    private KeyCode _runKeyCode = KeyCode.LeftShift;
    private KeyCode _attackKeyCode = KeyCode.J;

    /// <summary>
    /// 按下此按钮时，可以旋转摄像机。
    /// </summary>
    private const KeyCode _cameraAxis = KeyCode.Mouse1;

    /// <summary>
    /// 水平。
    /// </summary>
    public float Horizontal => Input.GetAxis("Horizontal");

    /// <summary>
    /// 垂直。
    /// </summary>
    public float Vertical => Input.GetAxis("Vertical");

    /// <summary>
    /// 按住（不放开）指定的按键。
    /// </summary>
    /// <param name="keyCode">按键。</param>
    /// <returns></returns>
    public static bool GetKey(KeyCode keyCode) => Input.GetKey(keyCode);

    /// <summary>
    /// 按下指定的按键。
    /// </summary>
    /// <param name="keyCode">按键。</param>
    /// <returns></returns>
    public static bool GetKeyDown(KeyCode keyCode) => Input.GetKeyDown(keyCode);

    /// <summary>
    /// 判断当前是不是正在按下“跑步”的按键。
    /// </summary>
    /// <returns></returns>
    public bool GetRunKey() => GetKey(_runKeyCode);

    /// <summary>
    /// 按下攻击键。
    /// </summary>
    /// <returns></returns>
    public bool GetAttackKeyDown() => GetKeyDown(_attackKeyCode);

    /// <summary>
    /// 判断当前是不是正在按下“摄像机旋转”的按键。
    /// </summary>
    /// <returns></returns>
    public static bool GetCameraAxis() => GetKey(_cameraAxis);
}