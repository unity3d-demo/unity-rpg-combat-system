using UnityEngine;

public class EnemyModel : MonoBehaviour
{
    private EnemyController _enemyController;

    private Animator _animator;

    /// <summary>
    /// 当前受伤动画索引。
    /// </summary>
    private int _currentHurtAnimationIndex = 1;

    public void Init(EnemyController enemyController)
    {
        _enemyController = enemyController;
        _animator = GetComponent<Animator>();
    }

    public void PlayHurtAnimation()
    {
        _animator.SetTrigger("Damage" + _currentHurtAnimationIndex);

        _currentHurtAnimationIndex = _currentHurtAnimationIndex == 1 ? 2 : 1;
    }

    public void StopHurtAnimation()
    {
        _animator.SetTrigger("DamageEnd");
    }
}