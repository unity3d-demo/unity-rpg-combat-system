using System;

using UnityEngine;

/// <summary>
/// 技能信息模型。
/// </summary>
[Serializable]
public class SettingSkillModel
{
    /// <summary>
    /// 技能配置数据。
    /// </summary>
    public SettingSkillData Data;

    /// <summary>
    /// 通过哪个按键触发。
    /// </summary>
    public KeyCode KeyCode;

    /// <summary>
    /// 冷却时间。
    /// </summary>
    public float CoolDownTime;

    /// <summary>
    /// 当前时间。
    /// </summary>
    /// <remarks>
    /// 用于检测冷却时间是否已运转完成。
    /// </remarks>
    public float CurrentTime;
}