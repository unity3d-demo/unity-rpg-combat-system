using UnityEngine;

public class PlayerMove : StateBase<PlayerState, PlayerController>
{
    private readonly float _walkSpeed = 1.60f;
    private readonly float _runSpeed = 10f;
    private readonly float _rotateSpeed = 20f;

    /// <summary>
    /// ֻ���ڰ�ס���ܲ���������ǰ������ֵ�Ǵ��� 0 ʱ���������ܲ���
    /// </summary>
    private bool IsRun => Controller.Input.GetRunKey() && (Controller.Input.Vertical != 0 || Controller.Input.Horizontal != 0);
    private float MoveSpeed => IsRun ? _runSpeed : _walkSpeed;

    public override void Init(IFSMController<PlayerState> controller, PlayerState stateType)
    {
        base.Init(controller, stateType);
    }

    public override void OnEnter()
    {
    }

    public override void OnExit()
    {
    }

    public override void OnUpdate()
    {
        Move(Controller.Input.Horizontal, Controller.Input.Vertical);

        if (Controller.CheckAttack())
        {
            Controller.ChangeState<PlayerAttack>(PlayerState.Attack);
        }
    }

    #region �ƶ�

    private void Move(float horizontal, float vertical)
    {
        #region �ƶ�

        //��������
        var inputDirection = new Vector3(horizontal, 0f, vertical);

        //up����
        var upAxis = -Physics.gravity.normalized;

        //������뷽������
        var moveDirection = CameraDirectionProcess(inputDirection, upAxis);

        Controller.CharacterController.SimpleMove(moveDirection * MoveSpeed);


        //// �ƶ��ķ���
        //var move = new Vector3(0, 0, vertical);

        //// ���� move �ǻ�����������ϵ�ģ�������Ҫͨ���˺���ת���ɽ�ɫ���������ϵ��
        //move = Controller.transform.TransformDirection(move);

        //// λ�ơ�
        //Controller.CharacterController.SimpleMove(move * MoveSpeed);

        #endregion

        #region ��ת

        if (inputDirection.magnitude > PlayerInput.INPUT_EPS)
            RotateProcess(Controller.transform, moveDirection, upAxis);

        //// ��ת
        //var rotate = new Vector3(0, horizontal, 0);
        //Controller.transform.Rotate(rotate * _rotateSpeed);

        #endregion

        var v = moveDirection.magnitude > 0 ? (IsRun ? 2 : 1) : 0;

        // ִ�ж�����
        Controller.Model.UpdateMove(0, v);
        //Controller.Model.UpdateMove(horizontal, vertical);
    }

    private Vector3 CameraDirectionProcess(Vector3 inputDirection, Vector3 upAxis)
    {
        //��ȡ�����
        var mainCamera = Camera.main;

        //��ͬ������up������
        var quat = Quaternion.FromToRotation(mainCamera.transform.up, upAxis);

        //ת��forward����
        var cameraForwardDirection = quat * mainCamera.transform.forward;

        //ת��������������
        var moveDirection = Quaternion.LookRotation(cameraForwardDirection, upAxis) * inputDirection.normalized;

        return moveDirection;
    }

    private void RotateProcess(Transform transform, Vector3 moveDirection, Vector3 upAxis)
    {
        //ͶӰ��upƽ����
        moveDirection = Vector3.ProjectOnPlane(moveDirection, upAxis);

        //�õ��ƶ�����������ת
        var playerLookAtQuat = Quaternion.LookRotation(moveDirection, upAxis);

        //���²�ֵ
        transform.rotation = Quaternion.Lerp(transform.rotation, playerLookAtQuat, _rotateSpeed * Time.deltaTime);
    }

    #endregion
}