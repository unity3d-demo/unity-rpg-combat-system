using System;
using System.Collections.Generic;

using UnityEngine;

public interface IFSMStateEntity<TStateType>
    where TStateType : Enum
{
    TStateType State { get; }

    void Init(IFSMController<TStateType> controller, TStateType stateType);

    /// <summary>
    /// 进入。
    /// </summary>
    void OnEnter();

    /// <summary>
    /// 更新。
    /// </summary>
    void OnUpdate();

    /// <summary>
    /// 退出。
    /// </summary>
    void OnExit();
}

public interface IFSMController<TStateType>
    where TStateType : Enum
{
    TStateType CurrentState { get; }

    void ChangeState<TStateEntity>(TStateType newState, bool isUpdateState = false)
        where TStateEntity : IFSMStateEntity<TStateType>, new();
}

/// <summary>
/// 有限状态机
/// </summary>
public abstract class FSMController<TStateType> : MonoBehaviour, IFSMController<TStateType>
    where TStateType : Enum
{
    protected IFSMStateEntity<TStateType> _currentStateObject;
    private readonly Dictionary<TStateType, object> _stateObjects = new Dictionary<TStateType, object>();

    public TStateType CurrentState { get; private set; }

    public void ChangeState<TStateEntity>(TStateType newState, bool isUpdateState = false) where TStateEntity : IFSMStateEntity<TStateType>, new()
    {
        if (newState.Equals(CurrentState) && !isUpdateState)
        {
            Debug.Log("test1");
            return;
        }

        if (_currentStateObject != null)
        {
            _currentStateObject.OnExit();
        }

        _currentStateObject = GetStateObject<TStateEntity>(newState);
        _currentStateObject.OnEnter();
    }

    private TStateEntity GetStateObject<TStateEntity>(TStateType stateType)
        where TStateEntity : IFSMStateEntity<TStateType>, new()
    {
        if (_stateObjects.ContainsKey(stateType))
            return (TStateEntity)_stateObjects[stateType];

        var state = new TStateEntity();

        state.Init(this, stateType);

        _stateObjects.Add(stateType, state);

        return state;
    }

    protected virtual void Update()
    {
        if (_currentStateObject != null) _currentStateObject.OnUpdate();
    }


}

public abstract class StateBase<TStateType, TFSMController> : IFSMStateEntity<TStateType>
    where TStateType : Enum
    where TFSMController : IFSMController<TStateType>
{
    public TStateType State { get; private set; }
    public TFSMController Controller { get; private set; }

    public virtual void Init(IFSMController<TStateType> controller, TStateType stateType)
    {
        Controller = (TFSMController)controller;
        State = stateType;
    }

    /// <summary>
    /// 进入。
    /// </summary>
    public abstract void OnEnter();

    /// <summary>
    /// 更新。
    /// </summary>
    public abstract void OnUpdate();

    /// <summary>
    /// 退出。
    /// </summary>
    public abstract void OnExit();
}