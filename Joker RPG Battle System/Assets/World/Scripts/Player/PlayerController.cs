using System;
using System.Collections;

using Cinemachine;

using DG.Tweening;

using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(CharacterController))]
public class PlayerController : FSMController<PlayerState>
{
    /// <summary>
    /// 普通攻击配置。
    /// </summary>
    [SerializeField]
    public SettingSkillData[] _standAttackSettings;
    private int _currentStandAttackIndex = 0;

    public SettingSkillModel[] _skillModels;

    /// <summary>
    /// 屏幕震动源。
    /// </summary>
    private CinemachineImpulseSource _currentStandImpulseSource;

    /// <summary>
    /// 摄像机注视的目标。
    /// </summary>
    private Transform _cameraTarget;

    /// <summary>
    /// 摄像机坐标。
    /// </summary>
    private Vector3 _cameraPosition;

    public PlayerInput Input { get; private set; }
    public PlayerAudio Audio { get; private set; }
    public PlayerModel Model { get; private set; }

    public CharacterController CharacterController { get; private set; }

    /// <summary>
    /// 当前是第几段普通攻击。
    /// </summary>
    public int CurrentStandAttackIndex
    {
        get => _currentStandAttackIndex;
        set
        {
            if (value >= _standAttackSettings.Length)
                value = 0;

            _currentStandAttackIndex = value;
        }
    }

    /// <summary>
    /// 当前正在使用的技能。
    /// </summary>
    public SettingSkillData CurrentSkill { get; private set; }

    private void Awake()
    {
        Input = new PlayerInput();
        Audio = new PlayerAudio(GetComponent<AudioSource>());
        CharacterController = GetComponent<CharacterController>();
        Model = transform.GetComponentInChildren<PlayerModel>();
        Model.Init(this);

        _currentStandImpulseSource = GetComponent<CinemachineImpulseSource>();
        _cameraTarget = transform.Find("CameraTarget");

        _cameraPosition = _cameraTarget.localPosition;
        ChangeState<PlayerMove>(PlayerState.Move);
    }

    /// <summary>
    /// 检测攻击状态。
    /// </summary>
    /// <returns></returns>
    public bool CheckAttack()
    {
        // 普攻检测。
        if (Input.GetAttackKeyDown() && Model.IsEquip && Model.CanSkillSwitch)
        {
            CurrentSkill = _standAttackSettings[CurrentStandAttackIndex];
            _attackAction = StandAttack;
            return true;
        }

        // 技能检测。
        if (_skillModels != null && _skillModels.Length > 0)
        {
            foreach (var skillModel in _skillModels)
            {
                if (skillModel == null)
                    continue;

                if (PlayerInput.GetKeyDown(skillModel.KeyCode) && Model.IsEquip && Model.CanSkillSwitch)
                {
                    CurrentSkill = skillModel.Data;
                    _attackAction = SkillAttack;
                    return true;
                }
            }
        }

        _attackAction = null;

        return false;
    }

    private Action _attackAction;

    public void Attack()
    {
        _attackAction?.Invoke();
    }

    /// <summary>
    /// 普通攻击。
    /// </summary>
    private void StandAttack()
    {
        Model.StartAttack(CurrentSkill);
        CurrentStandAttackIndex++;
    }

    /// <summary>
    /// 技能攻击。
    /// </summary>
    private void SkillAttack()
    {
        CurrentStandAttackIndex = 0;
        Model.StartAttack(CurrentSkill);
    }

    /// <summary>
    /// 播放声音。
    /// </summary>
    /// <param name="audioClip"></param>
    public void PlayAudio(AudioClip audioClip)
    {
        Audio.Play(audioClip);
    }

    /// <summary>
    /// 屏幕震动。
    /// </summary>
    public void ScreenImpulse()
    {
        _currentStandImpulseSource.GenerateImpulse();
    }

    /// <summary>
    /// 释放技能时产生的摄像机位移效果。
    /// </summary>
    /// <param name="offset">位移。</param>
    /// <param name="runTime">移动到目标位置总共需要消耗的时间。</param>
    /// <param name="backTime">返回原位总共需要消耗的时间。</param>
    public void CameraMoveForAttack(Vector3 offset, float runTime, float backTime)
    {
        _cameraTarget.DOLocalMove(_cameraPosition + offset, runTime).onComplete += () =>
        {
            _cameraTarget.DOLocalMove(_cameraPosition, backTime);
        };
    }

    /// <summary>
    /// 角色攻击时进行位移。
    /// </summary>
    public void CharacterAttackMove(Vector3 offset, float runTime)
    {
        StopCoroutine(DoCharacterAttackMove(transform.TransformDirection(offset), runTime));
        StartCoroutine(DoCharacterAttackMove(transform.TransformDirection(offset), runTime));
    }

    private IEnumerator DoCharacterAttackMove(Vector3 offset, float runTime)
    {
        var currentTime = 0f;

        while (currentTime < runTime)
        {
            // 计算单位移动的距离。
            var moveDirection = offset / runTime;

            CharacterController.SimpleMove(moveDirection);

            currentTime += Time.deltaTime;

            // 暂停一帧。
            yield return null;
        }
    }
}

public enum PlayerState
{
    None = 0,
    Move = 1,
    Attack = 2,
}