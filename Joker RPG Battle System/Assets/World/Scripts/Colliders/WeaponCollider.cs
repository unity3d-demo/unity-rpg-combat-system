using System.Collections.Generic;

using UnityEngine;

public class WeaponCollider : MonoBehaviour
{
    private SettingSkillHitData _hitData;

    [SerializeField]
    private BoxCollider _boxCollider;
    [SerializeField]
    private MeleeWeaponTrail _weaponTrail;
    private PlayerModel _model;

    /// <summary>
    /// 玩家本次攻击过的敌人列表（避免一次攻击造成多次伤害的问题）。
    /// </summary>
    private List<GameObject> _enemies;

    public void Init(PlayerModel model)
    {
        _model = model;
        _enemies = new List<GameObject>();
        EndAttack();
    }

    public void StartAttack(SettingSkillHitData hitData)
    {
        _hitData = hitData;

        // 启用碰撞。
        _boxCollider.enabled = true;

        // 启用拖尾。
        _weaponTrail.Emit = true;
    }

    public void EndAttack()
    {
        // 禁用碰撞。
        _boxCollider.enabled = false;

        // 关闭拖尾。
        _weaponTrail.Emit = false;

        // 清空本次已攻击的敌人列表。
        _enemies.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        // 如果碰撞到的是“敌人”且此次攻击第一次碰到它。
        if (other.tag == "Enemy" && !_enemies.Contains(other.gameObject))
        {
            // 输出伤害。
            _enemies.Add(other.gameObject);

            // 击中敌人时，如果开启了屏幕震动，则执行屏幕震动。
            if (_hitData.WantScreenImpulse)
            {
                _model.ScreenImpulse();
            }

            // 击中敌人时，是否开启屏幕效果。
            if (_hitData.WantScreenEffect)
            {
                PostProcessManager.Instance.ScreenEffect();
            }

            // 扣除敌人的 HP。
            var enemy = other.GetComponent<EnemyController>();
            if (enemy != null)
            {
                //GameObject obj1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
                //obj1.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                //Instantiate(obj1, other.ClosestPointOnBounds(transform.position), Quaternion.identity);
                enemy.Hurt(_hitData, _model.transform);

                if (_hitData.HitEffect != null)
                {
                    // 生成被击特效。
                    SpwanObjectByHit(_hitData.HitEffect.Spawn, GetHitPosition(other));

                    // 播放命中音效。
                    if (_hitData.HitEffect.AudioClip != null)
                        _model.PlayAudio(_hitData.HitEffect.AudioClip);
                }
            }
        }
    }

    /// <summary>
    /// 获得被攻击的位置坐标。
    /// </summary>
    /// <remarks>
    /// 此方法是想要获得攻击者的武器与被攻击者接触的点的位置坐标。
    /// </remarks>
    /// <param name="other">碰撞体。</param>
    /// <returns></returns>
    private Vector3 GetHitPosition(Collider other)
    {
        /*
         * ClosestPointOnBounds：到附加碰撞器的边界框最近的点。
         * 但是，由于传入的 transform.position 是武器所在的位置（也就是玩家角色手掌的位置）,因此没办法获得准确的坐标。
         * 所以，这里获得的 collisionPoint 是与 transform.position 距离最近的碰撞体（敌人角色）的坐标点（即：敌人身体中与手的位置最近的那个部位的坐标点）。
         * 注意：这里返回的是碰撞体的坐标。
         */
        var collisionPoint = other.ClosestPointOnBounds(transform.position);

        return collisionPoint;
    }

    /// <summary>
    /// 受到攻击时生成的对象。
    /// </summary>
    /// <param name="spawn">生成对象的数据。</param>
    /// <param name="spawnPoint">生成位置。</param>
    private void SpwanObjectByHit(SettingSkillSpawnObjectData spawn, Vector3 spawnPoint)
    {
        if (spawn == null)
            return;

        if (spawn.Prefab == null)
            return;

        var skillObject = Instantiate(spawn.Prefab, null);

        // 位置 = 碰撞体接触点 + 偏移量。
        skillObject.transform.position = spawnPoint + spawn.Position;
        // 生成的物体看向摄像机。
        skillObject.transform.LookAt(Camera.main.transform);
        // 旋转偏移。
        skillObject.transform.eulerAngles += spawn.Rotation;
        // 播放音效。
        _model.PlayAudio(spawn.AudioClip);
    }
}
