using System.Collections;

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessManager : MonoBehaviour
{
    public static PostProcessManager Instance;

    private Volume _volume;
    private ChromaticAberration _chromaticAberration;
    private DepthOfField _depthOfField;

    void Awake()
    {
        Instance = this;
        _volume = GetComponent<Volume>();
        _volume.profile.TryGet(out _chromaticAberration);
        _volume.profile.TryGet(out _depthOfField);
    }

    public void ScreenEffect()
    {
        StopCoroutine(StartCoroutineDepthOfFieldEffect());
        StartCoroutine(StartCoroutineDepthOfFieldEffect());
    }

    #region ����

    private IEnumerator StartCoroutineDepthOfFieldEffect()
    {
        while (_depthOfField.focalLength.value < 200f)
        {
            yield return new WaitForSeconds(0.01f);
            _depthOfField.focalLength.value += 10f;
        }

        yield return StopCoroutineDepthOfFieldEffect();
    }

    private IEnumerator StopCoroutineDepthOfFieldEffect()
    {
        while (_depthOfField.focalLength.value > 200f)
        {
            yield return new WaitForSeconds(0.01f);
            _depthOfField.focalLength.value -= 10f;
        }

        _depthOfField.focalLength.value = 50f;
    }

    #endregion

    #region ɫ��

    private void ChromaticAberrationEffect()
    {
        StopCoroutine(StartCoroutineChromaticAberrationEffect());
        StartCoroutine(StartCoroutineChromaticAberrationEffect());
    }

    private IEnumerator StartCoroutineChromaticAberrationEffect()
    {
        while (_chromaticAberration.intensity.value < 1f)
        {
            yield return new WaitForSeconds(0.01f);
            _chromaticAberration.intensity.value += 0.2f;
        }

        yield return StopCoroutineChromaticAberrationEffect();
    }

    private IEnumerator StopCoroutineChromaticAberrationEffect()
    {
        while (_chromaticAberration.intensity.value > 0f)
        {
            yield return new WaitForSeconds(0.01f);
            _chromaticAberration.intensity.value -= 0.2f;
        }
    }

    #endregion
}
