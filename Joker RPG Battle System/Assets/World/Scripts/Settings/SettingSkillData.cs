using System;

using UnityEngine;

/// <summary>
/// 技能配置数据。
/// </summary>
[CreateAssetMenu(fileName = "skilldata", menuName = "tszy/settings/skill/data")]
public class SettingSkillData : ScriptableObject
{
    /// <summary>
    /// 技能名称。
    /// </summary>
    public string Name;

    #region 角色技能动作的动画

    /// <summary>
    /// 动画参数-触发后执行技能动作。
    /// </summary>
    public string TriggerName;

    /// <summary>
    /// 动画参数-技能动作结束时触发（触发后回到待机）。
    /// </summary>
    public string OverTriggerName;

    #endregion

    /// <summary>
    /// 摄像机一次移动数据集合。
    /// </summary>
    public SettingCameraMoveData[] CameraMoveDatas;

    /// <summary>
    /// 角色位移数据集合。
    /// </summary>
    public SettingCharacterMoveData[] CharacterMoveDatas;

    /// <summary>
    /// 技能过程中任意时间生成物体（特效等）。
    /// </summary>
    public SettingSkillSpawnObjectData[] Spawns;

    /// <summary>
    /// 技能释放。
    /// </summary>
    public SettingSkillReleaseData ReleaseData;

    /// <summary>
    /// 一次技能生产多段攻击。
    /// </summary>
    public SettingSkillHitData[] HitDatas;
    public SettingSkillEndData EndData;
}

/// <summary>
/// 释放。
/// </summary>
[Serializable]
public class SettingSkillReleaseData
{
    /// <summary>
    /// 生成粒子。
    /// </summary>
    public SettingSkillSpawnObjectData Spawn;

    /// <summary>
    /// 音效。
    /// </summary>
    public AudioClip AudioClip;

    /// <summary>
    /// 是否可以旋转。
    /// </summary>
    /// <remarks>
    /// 比如：在释放技能的过程中旋转。
    /// </remarks>
    public bool CanRotate;
}

/// <summary>
/// 命中。
/// </summary>
[Serializable]
public class SettingSkillHitData
{
    /// <summary>
    /// 伤害值。
    /// </summary>
    public int DamageValue;

    /// <summary>
    /// 僵直时间。
    /// </summary>
    public float HardTime;

    /// <summary>
    /// 击退、击飞的力量。
    /// </summary>
    public Vector3 RepelVelocity;

    /// <summary>
    /// 击退、击飞的过渡时间。
    /// </summary>
    public float RepelTransitionTime;

    /// <summary>
    /// 是否需要屏幕抖动。
    /// </summary>
    public bool WantScreenImpulse;

    /// <summary>
    /// 是否需要屏幕特效。
    /// </summary>
    public bool WantScreenEffect;

    /// <summary>
    /// 命中效果。
    /// </summary>
    public SettingSkillHitEffectData HitEffect;

    /// <summary>
    /// 生成粒子。
    /// </summary>
    public SettingSkillSpawnObjectData Spawn;

    /// <summary>
    /// 音效。
    /// </summary>
    public AudioClip AudioClip;
}

/// <summary>
/// 结束。
/// </summary>
[Serializable]
public class SettingSkillEndData
{
    /// <summary>
    /// 生成粒子。
    /// </summary>
    public SettingSkillSpawnObjectData Spawn;
}

/// <summary>
/// 释放技能时生成的物体。
/// </summary>
[Serializable]
public class SettingSkillSpawnObjectData
{
    /// <summary>
    /// 预制体。
    /// </summary>
    public GameObject Prefab;

    /// <summary>
    /// 音效。
    /// </summary>
    public AudioClip AudioClip;

    /// <summary>
    /// 位置。
    /// </summary>
    public Vector3 Position;

    /// <summary>
    /// 旋转。
    /// </summary>
    public Vector3 Rotation;
}

/// <summary>
/// 一次摄像机移动。
/// </summary>
[Serializable]
public class SettingCameraMoveData
{
    /// <summary>
    /// 摄像机的偏移程度。
    /// </summary>
    public Vector3 Offset;

    /// <summary>
    /// 移动到目标位置需要消耗的时间。
    /// </summary>
    public float RunTime;

    /// <summary>
    /// 返回原位置需要消耗的时间。
    /// </summary>
    public float BackTime;
}

/// <summary>
/// 角色移动。
/// </summary>
[Serializable]
public class SettingCharacterMoveData
{
    /// <summary>
    /// 角色的偏移程度。
    /// </summary>
    public Vector3 Offset;

    /// <summary>
    /// 移动到目标位置需要消耗的时间。
    /// </summary>
    public float RunTime;
}