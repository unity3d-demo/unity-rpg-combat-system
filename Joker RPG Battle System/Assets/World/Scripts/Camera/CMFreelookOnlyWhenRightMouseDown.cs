using Cinemachine;

using UnityEngine;

/// <summary>
/// 设置只要按下鼠标右键才可以旋转视角。
/// </summary>
public class CMFreelookOnlyWhenRightMouseDown : MonoBehaviour
{
    private void Start()
    {
        CinemachineCore.GetInputAxis = GetAxisCustom;
    }

    //private void Update()
    //{
    //    CinemachineCore.GetInputAxis = GetAxisCustom;
    //}

    public float GetAxisCustom(string axisName)
    {
        if (axisName == "Mouse X")
        {
            if (PlayerInput.GetCameraAxis())
            {
                return UnityEngine.Input.GetAxis("Mouse X");
            }
            else
            {
                return 0;
            }
        }
        else if (axisName == "Mouse Y")
        {
            if (PlayerInput.GetCameraAxis())
            {
                return UnityEngine.Input.GetAxis("Mouse Y");
            }
            else
            {
                return 0;
            }
        }
        return 0;
    }
}