using UnityEngine;

/// <summary>
/// 技能命中时的效果配置。
/// </summary>
[CreateAssetMenu(fileName = "hitdata", menuName = "tszy/settings/skill/hit")]
public class SettingSkillHitEffectData : ScriptableObject
{
    /// <summary>
    /// 生成的粒子物体。
    /// </summary>
    public SettingSkillSpawnObjectData Spawn;

    /// <summary>
    /// 命中时产生的音效。
    /// </summary>
    public AudioClip AudioClip;
}