using UnityEngine;

public class PlayerAudio
{
    private AudioSource _audioSource;

    public PlayerAudio(AudioSource audioSource)
    {
        _audioSource = audioSource;
    }

    /// <summary>
    /// ����һ����Ч��
    /// </summary>
    /// <param name="clip"></param>
    public void Play(AudioClip clip)
    {
        if (clip == null)
            return;

        _audioSource.PlayOneShot(clip);
    }
}